import pandas as pd
from sklearn import model_selection
from apyori import apriori 

def load_data(file_name):
    data = pd.read_csv(file_name)
    return data


def preprocess(data):
    item_list = data.unstack().dropna().unique()
    print(item_list)
    print("% items", len(item_list))

    #print(data)
    #transform data into a list
    #item_list = data.join
    #item_list = data.unstack().dropna().unique()
    #print(item_list)
    #print("% items", len(item_list))

    train, test = model_selection.train_test_split(data, test_size=.10)
    print("train", train)
    print("test", test)

    #transform data .T transpose
    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()
    print("transform")
    #print(train)

    print("------------------------------")
    return train, test
    #for i in train[:10]:
    #    print(i)

def model(train_data, min_support=0.0045, min_confidence=0.2, min_lift=3, min_length=2):
    result = list(apriori(train_data, min_support=min_support, min_confidence=min_confidence, min_lift=min_lift, min_length=min_length))
    print("results")
    print(result)
    return result

def visulize(result):
    for rule in result:
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print("rule", components[0],"-->",components[1])
        print("support", rule[1])
        print("confidence", rule[2][0][2])
        print("lift", rule[2][0][3])
        print("------------------------------")

if __name__ == "__main__":
    data1 = load_data('store_data.csv')
    data2 = load_data('store2_data.csv')
    print(data1)
    print(data2)
    train, test = preprocess(data1)
    result = model(train)
    visulize(result)




